document.addEventListener("DOMContentLoaded", function() {
    var form = document.getElementById("form-data-diri");
    var submitButton = document.getElementById("submit");

    submitButton.addEventListener("click", function() {
        var nama = document.getElementById("nama").value;
        var jenisKelamin = document.querySelector('input[name="jenis_kelamin"]:checked').value;
        var tempatTinggal = document.getElementById("tempat_tinggal").value;
        alert("Nama : " + nama + "\nJenis Kelamin : " + jenisKelamin + "\nTempat Tinggal : " + tempatTinggal);
    });
});
